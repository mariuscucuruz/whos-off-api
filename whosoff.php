<?php
/**
 * @author Marius Cucuruz
 * @copyright 2017
 * 
 * attempt to use the WhosOff API
 * NOTE: The API is READ ONLY!
 * 
 * documentation regarding the API:
 * https://www.whosoff.com/features/api/
 * 
 * Available endpoints and descriptions:
 *   GET: /api/staff              #Retrieve staff details including tags and allowances
 *   GET: /api/department         #Retrieve department names
 *   GET: /api/tag                #Retrieve tag names
 *   GET: /api/free-restricted    #Retrieve free / restricted days
 *   GET: /api/leave-type         #Retrieve leave type names
 *   GET: /api/whosoff            #Retrieve leave details
 *   GET: /api/sandbox            #NO AUTHENTICATION REQUIRED  Returns Server Date Time, you IP address and Server Name
 *   GET: /api/sandbox-with-authentication    #As per /api/sandbox but authentication is required
 */
require_once ('whosoff/config.php');
$whosoff = new WhosoffAPI($config);

$api_action     = "staff";
$api_filters    = "start_date=01-". date('M-Y') ."&end_date=31-". date('M-Y') ."&leave_type=holiday";

    #$whosoff_staff = $whosoff->getStaff();
    #$whosoff_staff = $whosoff->submitRequest();
    $whosoff_staff = $whosoff->getWhosOff('cucuruz', 'bst');
    if ($whosoff_staff->results['http_code'] == 200)
    {
        echo "<ol>";
        foreach ($whosoff_staff->results['data'] as $responseArr)
        {
            echo "<li>".
                    "Employee: <strong>". $responseArr['First_Name'] ." ". $responseArr['Last_Name'] ."</strong> (#". $responseArr['Staff_Code'] .")<br />".
                    "Department: ".  @$responseArr['Department_Name'] ."<br />".
                    "Email Address: ".  @$responseArr['Email_Address'] ."<br />".
                    "Approver: ". $responseArr['Approver_First_Name'] ." ". $responseArr['Approver_Last_Name'] ."<br />";

                    # list tags if any:
                    if (count(@$responseArr['Tag']) > 0)
                        (count(@$responseArr['Tag']) . "Tag(s):".  implode(",", @$responseArr['Tag']) ."<br />");

                    # show notes if any:
                    if (@$responseArr['Notes'] != '')
                        ("Note(s):". $responseArr['Notes'] ."<br />");

            echo "<ul>";
            foreach (@$responseArr['Allowance'] as $allowances)
            {
                echo "<li style='list-style: none;'>".
                        "<strong>Allowance ". $allowances['Company_Year'] ."</strong>:<br />".
                        "Opening Balance: ". $allowances['Opening_Balance'] ." / ". $allowances['Remaining_Balance'] ." remaining<br />".
                        "Carried over: ". $allowances['Carryover'] ." / Lieu Time:". $allowances['Lieu_Time'] ."<br />".
                    "</li>";
            }
            echo "</ul>";
            echo "</li>";
        }
        echo "</ol>";
    }
    else {
            echo "<h2>Error #". $whosoff_staff['http_code'] ."</h2>\n";
    }

require_once('whosoff/debug.php');
