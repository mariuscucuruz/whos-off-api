<?php
/**
 * @author Marius Cucuruz
 * @copyright 2017
 * 
 * attempt to use the WhosOff API as a class
 * The API is READ ONLY!
 * 
 * documentation regarding the API:
 * https://www.whosoff.com/features/api/
 * 
 * Available endpoints and descriptions:
 *   GET: /api/staff              #Retrieve staff details including tags and allowances
 *   GET: /api/department         #Retrieve department names
 *   GET: /api/tag                #Retrieve tag names
 *   GET: /api/free-restricted    #Retrieve free / restricted details
 *   GET: /api/leave-type         #Retrieve leave type names
 *   GET: /api/whosoff            #Retrieve leave details
 *   GET: /api/sandbox            #NO AUTHENTICATION REQUIRED  Returns Server Date Time, you IP address and Server Name
 *   GET: /api/sandbox-with-authentication    #As per /api/sandbox but authentication is required
 */

class WhosoffAPI {

    # config array:
    private $config;

    # cURL options:
    private $curl_options;

    # req url
    protected $api_url;

    # filtering options:
    private $filters;

    # array filled w/ results:
    public $results;

    /**
     * Constructor.
     *
     * @param    array    $config (auth_key & url)
     */
    public function __construct ($config = FALSE) {

        if (!$config && count($config) > 1)
            return "no configuration passed";

        $this->filters = [];
        $this->results = [];

        $headers        = ["AUTH-KEY:" .$config['auth_key'], 'Content-type: application/json'];
        $this->api_url  = $config['url'];

        # define cURL options:
        $this->curl_options   = array(
                        CURLOPT_HEADER              => true,
                        CURLOPT_HTTPHEADER          => $headers,
                        CURLOPT_URL                 => $this->api_url,
                        CURLOPT_RETURNTRANSFER      => true,
                        CURLOPT_SSL_VERIFYHOST      => false,
                        CURLOPT_SSL_VERIFYPEER      => true,
                        CURLOPT_FRESH_CONNECT       => false, # try to cache
                        #CURLOPT_DNS_CACHE_TIMEOUT  => false, # seconds to cache DNS entries
                        CURLOPT_DNS_USE_GLOBAL_CACHE => false, # do NOT use a global DNS cache
                        CURLOPT_USERAGENT          => 'Apogee Corp'
                        );

    }

    /**
     * Method to test connection to API server
     *
     * @param    string    $field (field name from customer table)
     */
    public function test_conn () {

        $this->curl_handle      = curl_init();
        $this->curl_options[CURLOPT_URL] = $this->api_url .'staff';

        curl_setopt_array($this->curl_handle, $this->curl_options);

        $server_output          = curl_exec($this->curl_handle);
        $info                   = curl_getinfo($this->curl_handle);
        $header_size            = curl_getinfo($this->curl_handle, CURLINFO_HEADER_SIZE);
        $headers                = substr($server_output, 0, $header_size);
        $response               = substr($server_output, $header_size);
        $json_response          = json_decode($response, true);

        #echo "<pre>JSON response". print_r($json_response, true) ."</pre> \n";
        echo "<h2>HTTP Status Code : ". $info['http_code'] ."</h2>\n";

        if ($info['http_code'] == 200)
        {
            return TRUE;
        }
        else {
            return $info['http_code'];
        }
    }

    /**
     * Method to perform cURL request to API server
     *
     * @param    none
     * @return   array      (headers, JSON string)
     */
    public function doRequest () {

        #die ("<pre>options:". print_r($this->curl_options, true) ."</pre> \n");

        $this->curl_handle      = curl_init();
        curl_setopt_array($this->curl_handle, $this->curl_options);

        $server_output          = curl_exec($this->curl_handle);
        $info                   = curl_getinfo($this->curl_handle);
        $header_size            = curl_getinfo($this->curl_handle, CURLINFO_HEADER_SIZE);
        $headers                = substr($server_output, 0, $header_size);
        $response               = substr($server_output, $header_size);
        $json_response          = json_decode($response, true);

        # format response array with headers and data:
        $returnArr              = [
                                  'http_code' => $info['http_code'],
                                  'data'      => @$json_response['Data']
                                  ];

        $this->results = $returnArr;

        if ($info['http_code'] > 201)
            die ("".
                "<pre><strong>server output</strong>:".
                        print_r($server_output, true) ."</pre> \n".
                "<pre><strong>jSON response</strong>:".
                        print_r($json_response, true) ."</pre> \n".
                "<pre><strong>cURL options</strong>:".
                        print_r($this->curl_options, true) ."</pre>".
                "<pre><strong>results</strong>:".
                        print_r($this->results, true) ."</pre>".
                "");

        return $this;
    }

    /**
     * Method to list the staff
     *
     * @param    filters   optional
     * @return   $this
     */
    public function getStaff ($filters = NULL) {


        #sample: api/staff?email=random@&department_name=acc&company_year=2017
        $this->filters  = '?include=company,requester,stats';
        if ( is_array($filters) && count($filters)  > 0)
        {
            $this->filters .= implode('&', $filters);
        }
        else if ($filters  != '') 
        {
            $this->filters .= '&'. $filters;
        }
        #$this->filters    .= '?query="'. urlencode("my.api@apogeecorp.com") .'"';
        $this->filters     .= '&order_by=status&order_type=desc&per_page=250&page=1';
        $this->filters     .= '&include=company,requester,stats';

        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL] = $this->api_url .'staff'. $this->filters;



        # perform request:
        $this->doRequest();

        return $this;
    }



    /**
     * Method to fetch free/restricted days (eg bank holidays)
     * 
     * The value of `free_day` determins whether it's a Free  
     * or Restricted day (boolean: true or false)
     * 
     * Response example:
     *   [Data] => Array(
     *              [0] => Array (
     *                      [Description] => New Year`s Day (Substitute Day)
     *                      [Free_Day] => 1
     *                      [Department_Name] => All Departments
     *                      [Date] => 2017-01-02T00:00
     *                      [Created_Date] => 2016-01-07T13:12
     *                      ),
     *       ...
     *       )
     * 
     * @param    dept name      Optional    String / ID
     * @param    start_date     Optional    Date: dd-MMM-yyyy
     * @param    end_date       Optional    Date: dd-MMM-yyyy
     * @return   $this
     */
    public function getWhosOff($staff_email = NULL, $department_name = NULL, $start_date = NULL, $end_date = NULL) {

        $this_year  = date("Y");
        # use the DateTime object
        $dateObj    = new DateTime("01-01-$this_year");

        # parse start date:
        if (!$start_date)
        {
            # default start day Jan, 1
            $filterDateStart        = $dateObj->format('d-M-Y'); #date('d-M-Y');
        }
        else {
            #attemtp to format date string:
            $filterDateStart        = $dateObj->modify($start_date)->format('d-M-Y');

            # make sure to pass a valid date:
            if (!$filterDateStart)
                $filterDateStart    = $dateObj->format('d-M-Y'); #date('d-M-Y');
        }

        # parse end date:
        if (!$end_date)
        {
            # default end day Dec, 31
            $filterDateEnd          = $dateObj->modify("31-12-$this_year")->format('d-M-Y');
        }
        else {
            #attemtp to format date string:
            $filterDateEnd          = $dateObj->modify($end_date)->format('d-M-Y');

            # make sure to pass a valid date:
            if (!$filterDateEnd)
                $filterDateEnd      = $dateObj->modify("31-12-$this_year")->format('d-M-Y');
        }

        # set filters:
        $this->filters  = "?start_date=$filterDateStart&end_date=$filterDateEnd";
        
        # set filter dept if requested:
        if ($department_name)
            $this->filters .= "&department_name=$department_name";

        # set filter employee email if requested:
        if ($staff_email)
            $this->filters .= "&email=$staff_email";

        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL]  = $this->api_url .'whosoff'. $this->filters;

        # perform request:
        $this->doRequest();

        return $this;
    }


    /**
     * Method to fetch leave-type
     *
     * Response example:
     *       [Data] => Array(
     *           [0] => Array(
     *                   [Code] => H
     *                   [Name] => Holiday
     *                   [Colour] => #0099CC
     *                   [Requires_Approval] => 1
     *                   [Allowance_Time] => 1
     *               ),
     *       ...
     *       )
     * 
     * @param    none
     * @return   $this
     */
    public function getLeaveType () {

        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL]  = $this->api_url .'leave-type';

        # perform request:
        $this->doRequest();

        return $this;
    }

    /**
     * Method to fetch departments
     *
     * Response example:
     *       [Data] => Array(
     *           [0] => Array(
     *                   [Name] => BST - Admin
     *               ),
     *           [1] => Array(
     *                   [Name] => Sales
     *               ),
     *       ...
     *       )
     * 
     * @param    none
     * @return   $this
     */
    public function getDepartments() {

        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL]  = $this->api_url .'department';

        # perform request:
        $this->doRequest();

        return $this;
    }

    /**
     * Method to fetch free/restricted days (eg bank holidays)
     * 
     * The value of `free_day` determins whether it's a Free  
     * or Restricted day (boolean: true or false)
     * 
     * Response example:
     *   [Data] => Array(
     *              [0] => Array (
     *                      [Description] => New Year`s Day (Substitute Day)
     *                      [Free_Day] => 1
     *                      [Department_Name] => All Departments
     *                      [Date] => 2017-01-02T00:00
     *                      [Created_Date] => 2016-01-07T13:12
     *                      ),
     *       ...
     *       )
     * 
     * @param    dept name      Optional    String / ID
     * @param    start_date     Optional    Date: dd-MMM-yyyy
     * @param    end_date       Optional    Date: dd-MMM-yyyy
     * @return   $this
     */
    public function getSpecialDays($department_name = NULL, $start_date = NULL, $end_date = NULL) {

        $this_year  = date("Y");
        # use the DateTime object
        $dateObj    = new DateTime("01-01-$this_year");
        /*
        echo $dateObj->format('d-M-Y') ."<br />".
             $dateObj->modify("31-12-$this_year")
                     ->format('d-M-Y');
        */


        # parse start date:
        if (!$start_date)
        {
            # default start day Jan, 1
            $filterDateStart        = $dateObj->format('d-M-Y'); #date('d-M-Y');
        }
        else {
            #attemtp to format date string:
            $filterDateStart        = $dateObj->modify($start_date)->format('d-M-Y');

            # make sure to pass a valid date:
            if (!$filterDateStart)
                $filterDateStart    = $dateObj->format('d-M-Y'); #date('d-M-Y');
        }

        # parse end date:
        if (!$end_date)
        {
            # default end day Dec, 31
            $filterDateEnd          = $dateObj->modify("31-12-$this_year")->format('d-M-Y');
        }
        else {
            #attemtp to format date string:
            $filterDateEnd          = $dateObj->modify($end_date)->format('d-M-Y');

            # make sure to pass a valid date:
            if (!$filterDateEnd)
                $filterDateEnd      = $dateObj->modify("31-12-$this_year")->format('d-M-Y');
        }

        $this->filters  = "?start_date=$filterDateStart&end_date=$filterDateEnd";
        
        if ($department_name)
            $this->filters .= "&department_name=BST";

        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL]  = $this->api_url .'free-restricted'. $this->filters;

        # perform request:
        $this->doRequest();

        return $this;
    }



    /**
     * Method to add new holiday request
     *
     * @param    none
     * @return   doRequest result (JSON string)
     */
    public function submitRequest($conversation = NULL) {
        #if ($conversation == '')
        #    return FALSE;

#JCwq7aGsztkdfRgGtM2LSya48JWTJ2h1vMstvvtt
#UOeh9q78VuGI-guhwCslnuJewZYRdSced2stvvtt
#GGsU3V3jBPXB6TupHPGyHcMxZfQIGfq7nLsxswur
#LXZVMWFptqdwJtDGh9DLDzLdb3SFtPkCX1sxswuv
#FFNFpF60EhIbT6672ek5Zw7WJUxTVDtfKRrvuvxw
#IQ7MKpBNsJl-Xi6nfZHF9VISFypcDoslWHrvuvxw
#deeb96cc4f3dd4e9b7f5a88a933949ef

        unset($this->curl_options[CURLOPT_HTTPHEADER]);
        $this->curl_options[CURLOPT_POST]           = TRUE;
        $this->curl_options[CURLOPT_CUSTOMREQUEST]  = 'POST';
        $this->curl_options[CURLOPT_POSTFIELDS]     = json_encode([
                                                    '_proc'             => 'submitLeaveRequest',
                                                    'u'                => '265687',
                                                    '_pky'              => '45094',
                                                    '_leaveType'        => '45094',
                                                    '_leaveDays'        => '4',
                                                    '_daysSubmit'       => '4',
                                                    '_leaveDuration'    => '4',
                                                    '_leaveStartTime'   => '00:00',
                                                    '_message'          => 'msg: this is an API test',
                                                    '_notes'            => 'note: this is an attempt to hack the API',
                                                    '_startDate'        => '02/Mar/18 00:00',
                                                    '_endDate'          => '05/Mar/18 00:00',
                                                    '_days'             => '4',
                                                    '_allowanceDays'    => '22',
                                                    '_workingDays'      => '2',
                                                    '_'                 => '1515434344587',
                                                    ]);
        $this->curl_options[CURLOPT_URL]        = 'https://staff.whosoff.com/controls/leaverequest.ashx';

        $this->results = $this->doRequest();

        return $this;
    }


    /**
     * Destructor.
     *
     * @param    array    $config (auth_key & url)
     */
    public function __destruct () {

        if ($this->curl_handle)
            curl_close($this->curl_handle);
    }
}