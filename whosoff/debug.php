<?php
# make sure the config is included
require_once ('whosoff/config.php');

# instantiate object
$whosoff        = new WhosoffAPI($config);

# perform test
$api_response   = $whosoff->test_conn();

# show debug info
echo "<h2>Debug info shown below:</h2>\n".
     "HTTP Status Code : ". $api_response['http_code'] ."<br />\n".
     "<strong>Response Headers</strong> are:<br />\n".
     "<strong>Full JSON Response</strong>:<br />\n".
     "<pre>". print_r($api_response['data'], true) ."</pre>\n".
    "<br /><hr /><br />".
    "<pre>info:". print_r($api_response, true) ."</pre>".
    "---END---";
