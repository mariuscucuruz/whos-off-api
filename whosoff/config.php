<?php
/**
 * @author Marius Cucuruz
 * @copyright 2017
 * 
 * attempt to use the WhosOff API
 * 
 * documentation regarding the API:
 * https://www.whosoff.com/features/api/
 * 
 * Endpoint: https://wr1.whosoff.com/api/
 * 
 * Available endpoints and descriptions:
 *   GET: /api/staff              #Retrieve staff details including tags and allowances
 *   GET: /api/department         #Retrieve department names
 *   GET: /api/tag                #Retrieve tag names
 *   GET: /api/free-restricted    #Retrieve free / restricted details
 *   GET: /api/leave-type         #Retrieve leave type names
 *   GET: /api/whosoff            #Retrieve leave details
 *   GET: /api/sandbox            #NO AUTHENTICATION REQUIRED  Returns Server Date Time, you IP address and Server Name
 *   GET: /api/sandbox-with-authentication    #As per /api/sandbox but authentication is required
 */

include ('class.API.php');
## API config array:
$config = array(
            'auth_key'   => "xxxxxxx",
            'url'        => "https://wr1.whosoff.com/api/",
            );

#$whosoff = new WhosoffAPI($config);
#die("<pre>who's off obj:". print_r($whosoff->getStaff(), true) ."</pre>");
